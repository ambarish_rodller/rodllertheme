<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rodller
 */
?>

<?php get_template_part('template-parts/ads/before-footer'); ?>

<div id="rodller-prefooter">
	<?php get_template_part('template-parts/footer/pre'); ?>
</div>

<?php $footer_columns = rodller_get_footer_columns(); ?>
<?php if(!empty($footer_columns) && (
		is_active_sidebar('rodller-footer-column-sidebar-1') || is_active_sidebar('rodller-footer-column-sidebar-2') ||
		is_active_sidebar('rodller-footer-column-sidebar-3') || is_active_sidebar('rodller-footer-column-sidebar-4') || is_active_sidebar('rodller-footer-column-sidebar-5'))): ?>
	<footer id="rodller-main-footer">
		<div class="container">
			<div class="row">
				<?php foreach($footer_columns as $column_key => $column_width) :?>
					<?php $column_counter = $column_key + 1; ?>
					<div class="col-md-<?php echo esc_attr($column_width); ?> col-sm-12">
						<?php if(is_active_sidebar('rodller-footer-column-sidebar-' . $column_counter )): ?>
							<?php dynamic_sidebar( 'rodller-footer-column-sidebar-' . $column_counter );?>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
<?php $copyright_bar = rodller_get_option('footer_copyright_bar')  ?>
<?php $copyright_bar_content = rodller_get_option('footer_copyright_content')  ?>
<?php if($copyright_bar && !empty($copyright_bar_content)): ?>
    <div id="rodller-copyright">
		<?php echo wp_kses_post(wpautop(do_shortcode($copyright_bar_content))); ?>
    </div>
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>
